/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Utility methods to get download and upload URL of a file.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
'use strict';


/**
 * @param config the app configuration object
 */
module.exports = function(config) {

  function loadProvider(providerName) {
    providerName = providerName.toLowerCase();

    return require('./providers/' + providerName)(config);
  }

  return {
    /**
     * Create a download URL based upon the provider
     * @param file the File model
     * @param cb the callback (err, url)
     */
    getDownloadUrl: function(file, cb) {
      // Get storage provider
      var provider = loadProvider(file.storageLocation);
      provider.getDownloadURL(file.fileUrl, cb);
    },

    getUploadUrl: function(file, cb) {
      var provider = loadProvider(file.storageLocation);
      provider.getUploadURL(file.fileUrl, cb);
    }
  }
};