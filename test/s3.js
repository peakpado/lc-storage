/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Test s3 storage provider.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var assert = require('assert');

var config = {
  aws: {
    key: 'AWS_KEY',
    secret: 'AWS_SECRET',
    bucket: 'BUCKET',
    region: 'REGION'
  }
};
var storage = require('../')(config);


describe('local storage provider', function () {

  var file = {
    storageLocation: 's3',
    fileUrl: 'file-url'
  };

  it('should get download url', function (done) {
    storage.getDownloadUrl(file, function (err, url) {
      assert(!err);
      assert(url.match(/^https:\/\/s3.region.amazonaws.com\/BUCKET\/file-url\.*/));
      done();
    });
  });

  it('should get upload url', function (done) {
    storage.getUploadUrl(file, function (err, url) {
      assert(!err);
      assert(url.match(/^https:\/\/s3.region.amazonaws.com\/BUCKET\/file-url\.*/));
      done();
    });
  });

});