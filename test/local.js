/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Test local storage provider.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var assert = require('assert');

var config = {};
var storage = require('../')(config);


describe('local storage provider', function () {

  var file = {
    storageLocation: 'local',
    fileUrl: 'file-url'
  };
    
  it('should get download url', function (done) {
    storage.getDownloadUrl(file, function (err, url) {
      assert(!err);
      assert.equal(url, 'file-url');
      done();
    });
  });

  it('should get upload url', function (done) {
    storage.getUploadUrl(file, function (err, url) {
      assert(!err);
      assert(!url);
      done();
    });
  });

});