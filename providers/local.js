/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Local storage handler.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


/**
 * @param config the app configuration object
 */
module.exports = function(config) {

  return {
    getDownloadURL: function(fileUrl, cb) {
      cb(null, fileUrl);
    },
    getUploadURL: function(fileUrl, cb) {
      cb(null, null);
    }
  };
};