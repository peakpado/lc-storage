/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * S3 storage handler.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';

var aws = require('aws-sdk');


/**
 * @param config the app configuration object
 */
module.exports = function(config) {

  function getAWSUrl(operation, fileUrl, cb) {

    if (config.aws) {
      aws.config.update({
        accessKeyId: config.aws.key,
        secretAccessKey: config.aws.secret,
        region: config.aws.region
      });
    }

    var s3 = new aws.S3();
    var s3_params = {
      Bucket: config.aws.bucket,
      Key: fileUrl
    };
    s3.getSignedUrl(operation, s3_params, cb);
  }

  return {
    getDownloadURL: function(fileUrl, cb) {
      getAWSUrl('getObject', fileUrl, cb);
    },

    getUploadURL: function(fileUrl, cb) {
      getAWSUrl('putObject', fileUrl, cb);
    }
  };

};
