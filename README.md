lc-storage
===========

Utility methods to get download and upload URL of a file


## Installation
```npm install lc-storage```

## Usage

The storage provider S3 requires S3 credentials and configuration.

	var config = {
	  aws: {
	    key: 'AWS_KEY',
	    secret: 'AWS_SECRET',
	    bucket: 'BUCKET',
	    region: 'REGION'
	  }
	};
  
	var storage = require('lc-storage')(config);
	
	var file = {
	  storageLocation: 's3',
	  fileUrl: 'file-url'
	};
	
	storage.getDownloadUrl(file, function(err, url) {
		// process url
	});
	
	storage.getUploadUrl(file, function(err, url) {
		// process url
	});


## Test
To run the test, type `npm test`.

	$ npm test
	
## Authors

This library was developed by [peakpado](peakpado@gmail.com) at [TopCoder](http://www.topcoder.com)


## License

Copyright (c) 2014 TopCoder, Inc. All rights reserved.